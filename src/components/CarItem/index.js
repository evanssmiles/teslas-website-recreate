import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import styles from './styles'
import StyledButton from '../StyledButton'

const CarItem = (props) => {

    const { name, description, description2, image } = props.car;

    return (
        <View style={styles.carContainer}>
            <ImageBackground
                // source={require('../../assets/images/ModelX.jpeg')}
                source={image}
                style={styles.image}
            />

            <View style={styles.titles}>
                <Text style={styles.headtitle}>
                    {name}
                </Text>
                <Text style={styles.desc}>
                    {description}
                    {' '}
                    <Text style={styles.desc2}>
                        {description2}
                    </Text>
                </Text>

            </View>

            <View style={styles.buttonsContainer}>
                <StyledButton
                    type="primary"
                    content={"Custom Order"}
                    onPress={() => {
                        console.warn("custom order was pressed")
                    }}

                />

                <StyledButton
                    type="secondary"
                    content={"Existing Inventory"}
                    onPress={() => {
                        console.warn("existing inventory was pressed")
                    }}

                />
            </View>


        </View>
    );
};

export default CarItem;
