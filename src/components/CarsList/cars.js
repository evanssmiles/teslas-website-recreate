export default [{
  name: 'Model S',
  description: 'Starting at $69,420',
  image: require('../../assets/images/ModelS.jpeg'),
}, {
  name: 'Model 3',
  description: 'Order Online for',
  description2: 'Touchless Delivery',
  image: require('../../assets/images/Model3.jpeg'),
}, {
  name: 'Model X',
  description: 'Order Online for',
  description2: 'Touchless Delivery',
  image: require('../../assets/images/ModelX.jpeg'),
}, {
  name: 'Model Y',
  description: 'Order Online for',
  description2: 'Touchless Delivery',
  image: require('../../assets/images/ModelY.jpeg'),
}];
