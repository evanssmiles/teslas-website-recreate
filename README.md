This app is intended to recreate Tesla's website in Mobile device using React Native.

The main focus of this project are :

1. Render the text for title and subtitle
2. Render the background image
3. Implement button function
4. Render data using flatlist
5. Setup snap for view
6. Render the header
