import React from 'react'
import { View, Text, StyleSheet, StatusBar, ImageBackground } from 'react-native'
import CarsList from './src/components/CarsList'
import Header from './src/components/Header'

export default function App() {
  return (
    <View style={styles.container}>
      <Header />
      <CarsList />

      <StatusBar barStyle='auto' />
    </View >
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#20232A',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
